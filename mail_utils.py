import os

MAIL_CONFIG = {
    "smtp_host": os.getenv("SMTP_HOST"),
    "username": os.getenv("USERNAME"),
    "password": os.getenv("PASSWORD")
}

def createMail(sender, recipient, subject, html, text):
    '''
    A slightly modified version of Recipe #67083, included here 
    for completeness
    '''
    from email.mime.text import MIMEText
    from email.mime.multipart import MIMEMultipart
    htmlin = MIMEText(html, 'html')
    txtin = MIMEText(text, 'plain')
    msg = MIMEMultipart("alternative")
    msg["From"] = sender
    msg["To"] = recipient
    msg["Subject"] =  subject
    msg.attach(htmlin)
    msg.attach(txtin)
    return msg

# ---------------------------------------------------------------
def sendMail(sender, recipient, subject, html, text):
    import smtplib
    message = createMail(sender, recipient, subject, html, text)
    server = smtplib.SMTP(MAIL_CONFIG["smtp_host"])
    server.ehlo()
    server.starttls()
    server.login(MAIL_CONFIG["username"], MAIL_CONFIG["password"])
    server.sendmail(sender, recipient, message.as_string())
    server.quit()
