import urllib.request
import os
import re
import time
from bs4 import BeautifulSoup
import mysql.connector
import logging
from tables import Processed, session
import random #for user-agent randomization
import datetime

logger = logging.getLogger("uspto")


apc_daily_re = re.compile(r"(apc\d+\.zip)\"")
apc_annual_re = re.compile(r"(apc\d+\-\d+\-\d+.zip)\"")#Changed from original since format was different

PROCESSED_TABLE = "uspto_apc_processed"

BASE_URL = "https://bulkdata.uspto.gov/data/trademark/dailyxml/applications/"

user_agents = ['Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36',
               'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:66.0) Gecko/20100101 Firefox/66.0',
               'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36',
               'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36',
               'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36',
               'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',
               'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1 Safari/605.1.15',
               'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:66.0) Gecko/20100101 Firefox/66.0',
               'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:66.0) Gecko/20100101 Firefox/66.0',
               'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36']


def parse_web():
    req = urllib.request.Request(BASE_URL, data = None, headers = {'User-Agent': random.choice(user_agents)})
    list_files = []
    with urllib.request.urlopen(req) as response:
        html = BeautifulSoup(response)
        for hit in html.findAll('table'):
            for row in hit.findChildren('tr'):
                temp = row.findChildren('td')
                if(re.search(r"(apc\d+\.zip)", temp[0].text)):
                    date_time_obj = datetime.datetime.strptime(temp[2].text, "%Y-%m-%d %H:%M")
                    list_files.append([temp[0].text, date_time_obj.date()])
        
    
    return {
        "daily": list_files,
    }


def get_url(filename):
    return BASE_URL + filename


def get_fields_db():
    return map(lambda x: (x[0], x[1]), Processed.query\
            .with_entities(Processed.filename, Processed.upload_date)\
            .all())


def insert_rows(fields):
    rows = []
    for filename, kind, upload_date in fields:
        apc_file = Processed(filename, kind, upload_date)
        #logger.info("Add file: %s" % apc_file.filename)
        rows.append(apc_file)
    #logger.info("Bulk saving")
    session.bulk_save_objects(rows)
    session.commit()
    #logger.info("Bulk saved")


def compare_files(fields):
    files_db = list(get_fields_db())
    #logger.info("Files in DB: %s" % files_db)
    files_values = []
    if(len(files_db) == 0):
        listFiles = fields['daily']
        latestDate = listFiles[len(listFiles) - 1][1]
        for i in range(len(listFiles) - 1, -1, -1):
            if(listFiles[i][1] == latestDate):
                files_values.append(listFiles[i][0])
            else:
                break
    else:
        listFiles = fields['daily']
        latestDate = files_db[0][1]
        for i in range(len(listFiles) - 1, -1, -1):
            if(listFiles[i][1] > latestDate):
                files_values.append(listFiles[i][0])
            else:
                break
    '''
    files_values = []
    for k, v in fields.items():
        files_values.extend(v)
    '''
    db_file_names = [x[0] for x in files_db]
    #logger.info("Files in Web: %s" % files_values)
    #logger.info("Files in db: %s" % db_file_names)
    diff = set(files_values) - set(db_file_names)
    #logger.info("New files added are: %s" % diff)
    res = []
    for kind, filename_list in fields.items():
        for filename, upload_date in filename_list:
            if filename in diff:
                res.append((filename, kind, upload_date))
    if(len(diff) == 0):
        return None
    else:
        return res


def get_data_from_web():
    files = parse_web()    
    diff = compare_files(files)
    if diff != None:
        insert_rows(diff)
        return True
    else:
        #logger.info("No Files added")
        return False

if __name__ == "__main__":
    temp = parse_web()
    print(temp['daily'][0][1], temp['daily'][1][1])
    print(temp['daily'][0][1] < temp['daily'][1][1])
