from xml.etree import ElementTree as ET
from dateutil.parser import parse
import urllib.request
import zipfile
import sys
import cgitb
import logging
import os
import time
import shutil
from mail_utils import sendMail
from page import get_data_from_web, BASE_URL
from tables import Processed, session, conn, create_tables, MarkIdentificationSoundex
from tables import CaseFile, CaseFileOwner, Owner, ForeignApplications
from tables import CaseFileEventStatementCode
from tables import CaseFileStatement, CaseFileEventStatement, USPTO_Classification
from tables import Classification, Correspondent, DesignSearches, InternationalRegistration
from sqlalchemy import exc
from sqlalchemy.sql.expression import func
from xmlRecreation import recreate #Rearrange Files
import traceback #To mail back errors
import datetime

DEBUG = True

DATABASE = "uspto"

logger = logging.getLogger(DATABASE)
logger.disabled = True


def init_logger():
    """Init all logger parameters"""
    logger.setLevel(logging.DEBUG)
    # create a file handler
    log_name = "%s.log" % DATABASE
    # Handle log in the correct path if is windows or linux
    log_filename = log_name if os.name == "nt" else "/var/log/%s" % log_name
    # TODO change fileHandler with RotatingFieldHandler
    handler = logging.FileHandler(log_filename)
    handler.setLevel(logging.DEBUG)

    handler_stream = logging.StreamHandler()
    handler_stream.setLevel(logging.DEBUG)

    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    handler_stream.setFormatter(formatter)

    # add the handlers to the logger
    logger.addHandler(handler)
    logger.addHandler(handler_stream)


# case-file parsing fields definition
case_file = [
        "serial-number",
        "registration-number",
        "transaction-date",
        {
            "case-file-header": [
                "filing-date",
                "registration-date",
                "status-code",
                "status-date",
                "mark-identification",
                "published-for-opposition-date",
                'domestic-representative-name',
                "attorney-docket-number",
                "attorney-name",
                "mark-drawing-code",
                "principal-register-amended-in",
                "supplemental-register-amended-in"
                "trademark-in"
                "collective-trademark-in",
                "service-mark-in",
                "collective-service-mark-in",
                "collective-membership-mark-in",
                "certification-mark-in",
                "cancellation-pending-in",
                "published-concurrent-in",
                "concurrent-use-in",
                "concurrent-use-proceeding-in",
                "interference-pending-in",
                "opposition-pending-in",
                "section-12c-in",
                "section-2f-in",
                "section-2f-in-part-in",
                "renewal-filed-in",
                "section-8-filed-in",
                "section-8-partial-accept-in",
                "section-8-accepted-in",
                "section-15-acknowledged-in",
                "section-15-filed-in",
                "supplemental-register-in",
                "foreign-priority-in",
                "change-registration-in",
                "intent-to-use-in",
                "filed-as-use-application-in",
                "amended-to-use-application-in",
                "use-application-currently-in",
                "amended-to-itu-application-in",
                "filing-basis-filed-as-44d-in",
                "filing-basis-current-44d-in",
                "filing-basis-filed-as-44e-in",
                "amended-to-44e-application-in",
                "filing-basis-current-44e-in",
                "without-basis-currently-in",
                "filing-current-no-basis-in",
                "color-drawing-filed-in",
                "color-drawing-currently-in",
                "drawing-3d-filed-in",
                "drawing-3d-currently-in",
                "standard-characters-claimed-in",
                "filing-basis-filed-as-66a-in",
                "filing-basis-current-66a-in",
                "renewal-date",
                "law-office-assigned-location-code",
                "current-location",
                "location-date",
                "employee-name"
                ]
        },
        {
            "case-file-statements":
                [{
                    "case-file-statement":
                        [
                            "type-code",
                            "text"
                        ]
                }]
        },
        {
            "case-file-event-statements":
                [{
                    "case-file-event-statement":
                        [
                            "code",
                            "type",
                            "description-text",
                            "date",
                            "number"
                        ]
                }]
        },
        {
            "classifications":
                [{
                    "classification":
                        [
                            {
                                "international-codes":[
                                        "international-code"
                                    ]
                                },
                            {
                                "us-codes":[
                                    "us-code"
                                    ]
                            },
                            "status-code",
                            "status-date",
                            "first-use-anywhere-date",
                            "first-use-in-commerce-date",
                            "primary-code"
                        ]
                }]

        },
        {
            "case-file-owners":
                [{
                    "case-file-owner":
                        [
                            "legal-entity-type-code",
                            "entry-number",
                            "entity-statement",
                            {
                            "nationality":[
                                    "state",
                                    "country",
                                    "other"
                                ]
                            },
                            "party-name",
                            "party-type",
                            "address-1",
                            "city",
                            "state",
                            "postcode",
                            "country",
                            "dba-aka-text",
                            "composed-of-statement",
                            "name-change-explaination"
                        ]
                }]
        },
        {
            "correspondent":
                [
                    "address-1",
                    "address-2",
                    "address-3",
                    "address-4",
                    "address-5",
                    "email-1",
                    "email-2"
                ]
        },
        {
            "design-searches":[
                {
                    "design-search":[
                            "code"
                        ]
                    }
                ]
            },
        {
            "international-registration":[
                "international-registration-number",
                "international-registration-date",
                "international-publication-date",
                "international-renewal-date",
                "auto-protection-date",
                "international-death-date",
                "international-status-code",
                "international-status-date",
                "priority-claimed-in",
                "priority-claimed-date",
                "first-refusal-in"
                ]
            },
        {
            "foreign-applications":[
                {
                    "foreign-application":[
                        "filing-date",
                        "registration-date",
                        "registration-expiration-date",
                        "registration-renewal-date",
                        "registration-renewal-expiration-date",
                        "entry-number",
                        "application-number",
                        "country",
                        "other",
                        "registration-number",
                        "renewal-number",
                        "foreign-priority-claim-in"
                        ]
                    }
                ]
            },
        {
            "madrid-international-filing-requests":[
                {
                    "madrid-international-filing-record":[
                            "reference-number",
                            "original-filing-date-uspto",
                            "international-status-code",
                            "international-status-date",
                            "irregularity-reply-by-date",
                            "international-renewal-date",
                            {
                                "madrid-history-events":[
                                        "madrid-history-event",
                                        "code",
                                        "description-text",
                                        "date",
                                        "entry-number"
                                    ]
                                }
                        ]
                    }
                ]
            }
        ]


def fields_transform(case_field):
    """Receive the Case-Field Fields (easy to read)
    definition and transform to a more (easy to use)"""
    fields = {}
    for f in case_field:
        if isinstance(f, str):
            fields[f] = None
        elif isinstance(f, dict):
            for k in f.keys():
                fields[k] = fields_transform(f[k])
        elif isinstance(f, list):
            for k in f:
                if isinstance(k, dict):
                    fields['us-codes'] = fields_transform(k)
    return fields


def save_fields_max(tag_dict):
    for k, v in tag_dict.items():
        if v is not None and (isinstance(v, bool) == False):
            #if len(v) > 400:
                #logger.debug("Big names: %s" % tag_dict)
            if k in fields_length and len(v) > fields_length[k]:
                fields_length[k] = len(v)
            elif k not in fields_length:
                fields_length[k] = len(v)


def get_fields(element, element_list):
    """Receive element, element_list and return the
    data found in the list
    """
    data = {}
    for child in element:
        if child.tag in element_list.keys():
            element_value = element_list[child.tag]
            if element_value:
                #print(data)
                if child.tag not in data:
                    if element.tag.endswith("s"):
                        data[child.tag] = [get_fields(child, element_value)]
                    else:
                        data[child.tag] = get_fields(child, element_value)
                else:
                    '''
                    if(child.tag == "international-codes"):
                        print(data[child.tag])
                        print(get_fields(child, element_value))
                    '''
                    data[child.tag].append(get_fields(child, element_value))
            else:
                if element.tag != "nationality":
                    if child.tag == "us-code" or child.tag == "international-code":
                        if((child.tag) not in data):
                            data[child.tag] = []
                            data[child.tag].append(child.text)
                        else:
                            data[child.tag].append(child.text)
                    temp = child.tag.split("-")
                    if(child.text == "F" and temp[len(temp) - 1] == "in"):
                        child.text = False
                    elif(child.text == "T" and temp[len(temp) - 1] == "in"):
                        child.text = True
                    data[child.tag.replace("-", "_")] = child.text
                    save_fields_max({child.tag: child.text})
                else:
                    data["nationality_" + child.tag] = child.text
                    save_fields_max({child.tag: child.text})
    return data


def check_and_remove(case, transaction_date):
    print("Saved transaction date: %s" % case.transaction_date)
    print("New transaction str: %s" % transaction_date)
    parsed_date = parse(transaction_date)
    print("New transaction date: %s" % parsed_date.date())
    fkCheck0 = 'SET foreign_key_checks=0' #Otherwise cannot delete case since it is foreign key for other tables
    fkCheck1 = 'SET foreign_key_checks=1'

    if (case.transaction_date <= parsed_date.date()):#Update
        conn.execute(fkCheck0)
        session.query(CaseFile).filter(CaseFile.serial_number == case.serial_number).update({"transaction_date": (transaction_date)})
        #session.commit()
        conn.execute(fkCheck1)
        return True
    elif (case.transaction_date > parsed_date.date()):#Redundant data
        return False


def save_case_file(data):
    c = CaseFile()
    objects_to_save = []
    serial_number = data["serial_number"]
    case = CaseFile.query.get(serial_number)
    flag = 0
    if case:
        check = check_and_remove(case, data["transaction_date"])
        if(check == False):
            return
        flag = 1
    for key, value in data.items():
        valueCorres = data['correspondent']
        correspondentPK = 1
        if(len(valueCorres) != 0 and ('pk' not in valueCorres.keys())):
            checkExistsCorres = session.query(Correspondent).filter_by(address_1 = valueCorres['address_1']).first()
            if(checkExistsCorres):
                correspondentPK = checkExistsCorres.pk
            else:
                corres = Correspondent()
                corres.__dict__.update(valueCorres)
                session.add(corres)
                session.commit()
                session.flush()
                session.refresh(corres)
                correspondentPK = corres.pk
        else:
            checkNullBaseExists = session.query(Correspondent).filter_by(pk = '1').first()

            if(checkNullBaseExists):
                pass
            else:
                corres = Correspondent()
                valueCorres['pk'] = correspondentPK
                corres.__dict__.update(valueCorres)
                session.add(corres)
                session.commit()

        if isinstance(value, dict):
            if key == "case-file-header":
                if flag == 0:
                    setattr(c, "correspondent", correspondentPK)
                    for keyHeader, valueHeader in value.items():
                        setattr(c, keyHeader, valueHeader)
                    if('mark_identification' in value.keys()):
                        if(value['mark_identification'] != ''):
                            checkSerial = MarkIdentificationSoundex.query.get(c.serial_number)
                            if(checkSerial):
                                session.query(CaseFile).filter(CaseFile.serial_number == case.serial_number).update({"mark_identification": value['mark_identification'],
                                                                                                                    "soundex" : session.query(func.soundex(value['mark_identification']))})
                            else:
                                soundex = MarkIdentificationSoundex()
                                soundexDict = {'serial_number' : c.serial_number,
                                               'mark_identification' : value['mark_identification'],
                                               'soundex' : session.query(func.soundex(value['mark_identification']))}
                                soundex.__dict__.update(soundexDict)
                                session.add(soundex)
                                session.commit()
                    session.add(c)
                    session.commit()
                else:
                    session.query(CaseFile).filter(CaseFile.serial_number == case.serial_number).update(value)
            elif key == "case-file-statements":
                for cf_statement in value[key[:-1]]:
                    cf_statement_obj = CaseFileStatement()
                    if "text" in cf_statement:
                        cf_statement["text"] = cf_statement["text"][:10000]
                    cf_statement["type_code_prefix"] = cf_statement["type_code"][0:2]
                    cf_statement_obj.__dict__.update(cf_statement)
                    objects_to_save.append(cf_statement_obj)
            elif key == "case-file-event-statements":
                startPoint = -1
                if(flag == 1):
                    startPoint = session.query(func.max(CaseFileEventStatement.number)).filter(CaseFileEventStatement.serial_number == case.serial_number).scalar()
                    if(startPoint == None):
                        startPoint = -1

                temp = value[key[:-1]]
                
                for cfe_statement in temp[startPoint + 1: ]:
                    cfe_statement_obj = CaseFileEventStatement()
                    cfe_statement_obj.__dict__.update(cfe_statement)
                    objects_to_save.append(cfe_statement_obj)
                    findCode = session.query(CaseFileEventStatementCode).filter_by(code = cfe_statement['code']).first() #Find if code already exists
                    if(findCode):
                        pass
                    else:
                        code_obj = CaseFileEventStatementCode()
                        code_dict = {"code" : cfe_statement['code'],
                                     "type" : cfe_statement['type'],
                                     "description_text" : cfe_statement['description_text']}
                        code_obj.__dict__.update(code_dict)
                        session.add(code_obj)
                        session.commit()

            elif key == "classifications":
                if(flag == 1):
                    delete_q1 = Classification.__table__.delete().where(Classification.serial_number == case.serial_number)
                    delete_q2 = USPTO_Classification.__table__.delete().where(USPTO_Classification.serial_number == case.serial_number)
                    session.execute(delete_q1)
                    session.execute(delete_q2)
                    session.commit()

                for classification in value[key[:-1]]:
                    tempCodes = classification['us-codes']
                    tempInternationalCodes = classification['international-codes']
                    del(classification['us-codes'])
                    del(classification['international-codes'])

                    checkEmpty = Classification.query.get("1")
                    classification_entry_number = 0
                    classKey = None
                    if(checkEmpty):
                        classification_entry_number = session.query(func.max(Classification.classification_entry_number)).scalar()
                    if("international-code" in tempInternationalCodes.keys()):
                        for international_code in tempInternationalCodes['international-code']:
                            classification_obj = Classification()
                            classification_obj.__dict__.update(classification)
                            classification_obj.serial_number = c.serial_number
                            classification_obj.international_code = international_code
                            classification_obj.classification_entry_number = classification_entry_number + 1
                            session.add(classification_obj)
                            session.flush()
                            session.refresh(classification_obj)
                        classKey = classification_entry_number + 1
                    #print("tmpCode:", tempCodes)
                    if('us-code' in tempCodes.keys()):
                        for us_code in tempCodes['us-code']:
                            tempDict = {'classification_entry_number' : classKey, 'us_code' : us_code} #For uspto codes
                            uspto_class = USPTO_Classification()
                            uspto_class.__dict__.update(tempDict)
                            uspto_class.serial_number = c.serial_number
                            session.add(uspto_class)
                            session.commit()
                            session.flush()
                            session.refresh(uspto_class)


            elif key == "case-file-owners":
                if(flag == 1):
                    delete_q1 = CaseFileOwner.__table__.delete().where(CaseFileOwner.case_file == case.serial_number)
                    session.execute(delete_q1)
                    session.commit()

                for owner in value[key[:-1]]:
                    owner_obj = Owner()
                    tempDict = {'case_file' : c.serial_number, 'entry_no' : owner['entry_number'], 'party_type' : owner['party_type']}
                    del(owner['entry_number'])
                    del(owner['party_type'])
                    if("nationality" in owner.keys()):
                        tempNationality = owner['nationality'] #Used to extract state, country or other from nationality tag
                        owner.update(tempNationality)
                        del(owner['nationality'])
                    owner_key = 0
                    checkExists = session.query(Owner).filter_by(party_name = owner['party_name']).first()
                    if(checkExists):
                        owner_key = checkExists.pk
                    else:
                        owner_obj.__dict__.update(owner)
                        owner_obj.serial_number = c.serial_number
                        session.add(owner_obj)
                        session.commit()
                        session.flush()
                        session.refresh(owner_obj)
                        owner_key = owner_obj.pk
                    tempDict['owner'] = owner_key #PK for owner, goes to mapping table
                    case_owner = CaseFileOwner()
                    case_owner.__dict__.update(tempDict)
                    session.add(case_owner)
                    session.commit()

            elif key == "design-searches":
                if(flag == 1):
                    delete_q1 = DesignSearches.__table__.delete().where(DesignSearches.serial_number == case.serial_number)
                    session.execute(delete_q1)
                    session.commit()
                    
                for valueDes in value[key[:-2]]:
                    design_searches = DesignSearches()
                    design_searches.__dict__.update(valueDes)
                    objects_to_save.append(design_searches)

            elif key == "international-registration":
                if(flag == 1):
                    session.query(InternationalRegistration).filter(InternationalRegistration.serial_number == case.serial_number).update(value)
                else:
                    international_reg = InternationalRegistration()
                    international_reg.__dict__.update(value)
                    objects_to_save.append(international_reg)

            elif key == "foreign-applications":
                if(flag == 1):
                    routes_query = ForeignApplications.query.with_entities(ForeignApplications.application_number).filter(ForeignApplications.serial_number == case.serial_number).all()
                    #result = conn.execute(routes_query)
                    resList = [r[0] for r in routes_query]
                    for valueFor in value[key[:-1]]:
                        if('application_number' in valueFor.keys()):
                            if(valueFor['application_number'] not in resList):
                                foreign_applications = ForeignApplications()
                                foreign_applications.__dict__.update(valueFor)
                                objects_to_save.append(foreign_applications)
                            else:
                                session.query(ForeignApplications).filter(ForeignApplications.application_number == valueFor['application_number']).update(valueFor)
                        else:
                            foreign_applications = ForeignApplications()
                            foreign_applications.__dict__.update(valueFor)
                            objects_to_save.append(foreign_applications)
                else:
                    for valueFor in value[key[:-1]]:
                        foreign_applications = ForeignApplications()
                        foreign_applications.__dict__.update(valueFor)
                        objects_to_save.append(foreign_applications)

        else:
            setattr(c, key, value)
    #session.add(c)
    #session.commit()
    for o in objects_to_save:
        o.serial_number = c.serial_number
    session.bulk_save_objects(objects_to_save)
    session.commit()


def parse_xml(filename, fields, processed_ins=None):
    """Main Parsing function"""
    #logger.info("Fields: %s" % fields)
    #logger.info("Fields.keys: %s" % fields.keys())
    parser = ET.iterparse(filename, events=("end", ))
    cnt = 0
    if processed_ins and processed_ins.last_index:
        start_on = processed_ins.last_index
    else:
        start_on = 0
    for _, element in parser:
        if element.tag == "case-file":
            if cnt >= start_on:
                data = get_fields(element, fields)
                now = time.time()
                save_case_file(data)
                #session.add(row)
                try:
                    if processed_ins:
                        processed_ins.last_index = cnt
                        session.add(processed_ins)
                    session.commit()
                    #print("time elapsed: %s" % (time.time() - now))
                except exc.IntegrityError:
                    #logger.warning("This is a duplicated entry dont worry maybe this is a retry")
                    session.rollback()
                #logger.info("%s rows processed" % cnt)
            cnt += 1
            #if cnt % 5000 is 0:
                #logger.warning("%s rows processed" % cnt)
            element.clear()
            #if(cnt == 1000):
            #   break
        processed_ins.processed_date = datetime.datetime.now().date()
    #logger.debug("Fields length: %s" % fields_length)
    #logger.info("Finished processing quantity: %s" % cnt)


def download(filename):
    zip_url = "%s%s" % (BASE_URL, filename)
    #logger.info("Download %s" % zip_url)
    if not os.path.exists(filename):
        tmp_filename = "%s.tmp" % filename
        urllib.request.urlretrieve(zip_url, tmp_filename)
        #logger.info("Downloaded on %s" % tmp_filename)
        shutil.move(tmp_filename, filename)
        #logger.info("Copied %s to %s" % (tmp_filename, filename))


def extract(filename):
    zip_ref = zipfile.ZipFile(filename)
    f_name = zip_ref.filelist[0].filename
    if not os.path.exists(f_name):
        #logger.info("Decompressing %s" % filename)
        zip_ref.extractall(".")
        #logger.info("Decompressed to %s" % f_name)
    zip_ref.close()
    return f_name


"""
RUNNING IN A FILE WITH 47683
Fields length: {
    'serial-number': 8,
    'registration-number': 7,
    'transaction-date': 8,
    'filing-date': 8,
    'status-code': 3,
    'status-date': 8,
    'mark-identification': 1727,
    'mark-drawing-code': 4,
    'published-for-opposition-date': 8,
    'domestic-representative-name': 100,
    'attorney-name': 428,
    'opposition-pending-in': 1,
    'international-code': 3,
    'us-code': 3,
    'class_status-code': 1,
    'class_status-date': 8,
    'first-use-anywhere-date': 8,
    'first-use-in-commerce-date': 8,
    'primary-code': 3,
    'address-1': 40,
    'address-2': 40,
    'address-3': 40,
    'address-4': 40,
    'address-5': 40}
"""
fields_length = {}

##Function to give multiple order-by columns as sqlalchemy doesn't natively support multiple columns in order_by function
def sortOrders():
    return (Processed.kind.desc(), Processed.filename.desc())

def main():
    """Main Function that handle all errors
    ocurred and sendMail if needed
    """
    init_logger()
    filename = None
    try:
        filename = sys.argv[1]
    except IndexError:
        pass
    if filename:
        fields = fields_transform(case_file)
        recreate(filename, filename)
        parse_xml(filename, fields)
    else:
        # Diff
        existingFile = get_data_from_web()
        if(existingFile == True):
            #logger.info("Clearing old database")
            session.query(MarkIdentificationSoundex).delete()
            session.query(CaseFileStatement).delete()
            session.query(CaseFileEventStatement).delete()
            session.query(USPTO_Classification).delete()
            session.query(Classification).delete()
            session.query(CaseFileOwner).delete()
            session.query(Owner).delete()
            session.query(DesignSearches).delete()
            session.query(InternationalRegistration).delete()
            session.query(ForeignApplications).delete()
            #session.query(Processed).delete()
            session.query(CaseFile).delete()
            session.query(Correspondent).delete()
            #logger.info("Old database cleared")
        
        #logger.info("Starting uspto script")
        results = Processed.query.filter(Processed.processed == False).order_by(*sortOrders()).all()
        fields = fields_transform(case_file)
        if results:
            for r in results:
                download(r.filename)
                decompressed = extract(r.filename)
                #logger.info("Rearranging XML")
                recreate(decompressed, decompressed)
                #logger.info("Rearrangement Complete")
                parse_xml(decompressed, fields, processed_ins=r)
                r.processed = True
                os.remove(r.filename)
                os.remove(decompressed)
                session.add(r)
                session.commit()
        else:
            pass
            #logger.info("Nothing to all files are proccessed")
    #logger.info("Finished uspto script")


if __name__ == "__main__":
    try:
        create_tables()
        main()
    except Exception as e:

        import smtplib
        # creates SMTP session
        s = smtplib.SMTP('in-v3.mailjet.com', '587')
        # start TLS for security
        s.set_debuglevel(True)
        s.starttls()

        # Authentication
        x = s.login("2be0dd12f4bfcd560a8daa7cfff39eb1", "e3e1a218ac109f8a051f1dee1e542958")

        # message to be sent
        fromAddress = "noreply@iphawk.info"
        toAddress = "saurabh.karalkar1869@gmail.com"
        em = traceback.format_exc() + "\nFrom daily database"
        message = "From: {}\r\nTo: {}\r\n\r\n{}\r\n".format(fromAddress, toAddress, em)

        # sending the mail
        x = s.sendmail("noreply@iphawk.info", "saurabh.karalkar1869@gmail.com", message)

        # terminating the session
        s.quit()

        #logger.error("This is an unhandled error", exc_info=1)
        print("This is an error please check an deploy")

        traceback.print_exc()
        '''
        while True:
            time.sleep(1)
        '''
