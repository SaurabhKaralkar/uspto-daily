# Create Dockerfile here
FROM python:alpine

WORKDIR /usr/src/app

# Override the nginx start from the base container
COPY *.py ./
COPY reference_tables ./reference_tables
COPY requirements.txt ./
COPY start.sh /start.sh 
RUN chmod +x /start.sh \
&& pip install -r requirements.txt
ENTRYPOINT ["/start.sh"]
