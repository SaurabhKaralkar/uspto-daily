import xml.etree.ElementTree as ET

def recreate(filename, outputname):
    """Function used to rearrange XML in order to club together multiple
    instances of international codes and US codes under respective parent element
    """
    parser = ET.parse(filename)
    for element in parser.iter('classification'):
        us_codes = []
        international_codes = []
        ##Club all international codes in a parent
        #print("Forming list of international codes")
        for child in element.findall('international-code'):
                international_codes.append(child.text)
                element.remove(child)
        #print("Clubbing international codes")
        newElem = ET.Element('international-codes')
        for i in range(len(international_codes)):
            temp = ET.SubElement(newElem, "international-code")
            temp.text = international_codes[i]
            
        element.insert(2, newElem)

        ##Club all us codes in a parent
        #print("Forming list of US codes")
        for child in element.findall('us-code'):
                us_codes.append(child.text)
                element.remove(child)
        #print("Clubbing US codes")
        newElem = ET.Element('us-codes')
        for i in range(len(us_codes)):
            temp = ET.SubElement(newElem, "us-code")
            temp.text = us_codes[i]
            
        element.insert(3, newElem)
    parser.write(outputname)

if __name__ == "__main__": ##For testing
    recreate("testXml.xml", "newXml.xml")
