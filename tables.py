from sqlalchemy import Column, Integer, String, Boolean, Text
from sqlalchemy import Date, ForeignKey, create_engine
from sqlalchemy import Table
from sqlalchemy.orm import relationship, sessionmaker, scoped_session
from sqlalchemy.ext.declarative import declarative_base
import csv

Base = declarative_base()
#URI = "mysql+mysqlconnector://root:@127.0.0.1:3306/school"
#URI = "mysql+mysqlconnector://iphawk:iphawk1234@usptodatatest.cjw45nzbsyzr.us-east-1.rds.amazonaws.com:3306/usptotest2"
URI = "mysql+mysqlconnector://iphawk:iphawk1234@uspto-daily.cjw45nzbsyzr.us-east-1.rds.amazonaws.com:3306/uspto_daily"
engine = create_engine(URI)
conn = engine.connect()
session = scoped_session(sessionmaker(bind=engine))

metadata = Base.metadata
Base.query = session.query_property()


class CaseFile(Base):
    __tablename__ = "case_file"
    serial_number = Column(Integer, primary_key=True)
    registration_number = Column(Integer)
    transaction_date = Column(Date)
    ##Newly Added
    filing_date = Column(Date)
    registration_date = Column(Date)
    status_code = Column(String(3))
    status_date = Column(Date)
    mark_identification = Column(Text)
    published_for_opposition_date = Column(Date)
    domestic_representative_name = Column(Text)
    attorney_docket_number = Column(String(20))
    attorney_name = Column(Text)
    mark_drawing_code = Column(String(50))
    ##Fields not in JSON
    principal_register_amended_in = Column(Boolean)
    supplemental_register_amended_in = Column(Boolean)
    trademark_in = Column(Boolean)
    collective_trademark_in = Column(Boolean)
    service_mark_in = Column(Boolean)
    collective_service_mark_in = Column(Boolean)
    collective_membership_mark_in = Column(Boolean)
    certification_mark_in = Column(Boolean)
    cancellation_pending_in = Column(Boolean)
    published_concurrent_in = Column(Boolean)
    concurrent_use_in = Column(Boolean)
    concurrent_use_proceeding_in = Column(Boolean)
    interference_pending_in = Column(Boolean)
    opposition_pending_in = Column(Boolean)
    section_12c_in = Column(Boolean)
    section_2f_in = Column(Boolean)
    section_2f_in_part_in = Column(Boolean)
    renewal_filed_in = Column(Boolean)
    section_8_filed_in = Column(Boolean)
    section_8_partial_accept_in = Column(Boolean)
    section_8_accepted_in = Column(Boolean)
    section_15_acknowledged_in = Column(Boolean)
    section_15_filed_in = Column(Boolean)
    supplemental_register_in = Column(Boolean)
    foreign_priority_in = Column(Boolean)
    change_registration_in = Column(Boolean)
    intent_to_use_in = Column(Boolean)
    filed_as_use_application_in = Column(Boolean)
    amended_to_use_application_in = Column(Boolean)
    use_application_currently_in = Column(Boolean)
    amended_to_itu_application_in = Column(Boolean)
    filing_basis_filed_as_44d_in = Column(Boolean)
    filing_basis_current_44d_in = Column(Boolean)
    filing_basis_filed_as_44e_in = Column(Boolean)
    amended_to_44e_application_in = Column(Boolean)
    filing_basis_current_44e_in = Column(Boolean)
    without_basis_currently_in = Column(Boolean)
    filing_current_no_basis_in = Column(Boolean)
    color_drawing_filed_in = Column(Boolean)
    color_drawing_currently_in = Column(Boolean)
    drawing_3d_filed_in = Column(Boolean)
    drawing_3d_current_in = Column(Boolean)
    standard_characters_claimed_in = Column(Boolean)
    filing_basis_filed_as_66a_in = Column(Boolean)
    filing_basis_current_66a_in = Column(Boolean)
    renewal_date = Column(Date)
    law_office_assigned_location_code = Column(String(3))
    ###
    current_location = Column(String(100))
    location_date = Column(Date)
    ##new
    employee_name = Column(String(30))
    correspondent = Column(Integer, ForeignKey('correspondent.pk'))
    ##
    #headers = relationship("CaseFileHeader", backref="user", cascade="all, delete-orphan")
    statements = relationship("CaseFileStatement", backref="user", cascade="all, delete-orphan")
    event_statements = relationship("CaseFileEventStatement", backref="user", cascade="all, delete-orphan")
    classifications = relationship("Classification", backref="user", cascade="all, delete-orphan")
    owner = relationship("CaseFileOwner", backref="user", cascade="all, delete-orphan")
    #correspondent = relationship("Correspondent", backref="user", cascade="all, delete-orphan")
    uspto_classifications = relationship("USPTO_Classification", backref="user", cascade="all, delete-orphan")
    desg = relationship("DesignSearches", backref="user", cascade="all, delete-orphan")
    intReg = relationship("InternationalRegistration", backref="user", cascade="all, delete-orphan")
    foreignApp = relationship("ForeignApplications", backref="user", cascade="all, delete-orphan")

class MarkIdentificationSoundex(Base):
    __tablename__ = "map_markIdentification_soundex"
    serial_number = Column(Integer, primary_key = True)
    mark_identification = Column(String(20))
    soundex = Column(String(30))

class StatusCode(Base):
    __tablename__ = "ref_status_code"
    code = Column(Integer, primary_key=True, autoincrement=False)
    status = Column(String(50))
    definition = Column(String(100))

###New Classes for uspto and nice codes
class USPTOCode(Base):
    __tablename__ = "ref_uspto_code"
    code = Column(String(10), primary_key = True)
    definition = Column(String(100))

class NiceCode(Base):
    __tablename__ = "ref_nice_code"
    code = Column(String(3), primary_key = True)
    definition = Column(Text)

class CaseFileStatement(Base):
    __tablename__ = "case_file_statement"
    pk = Column(Integer, primary_key=True)
    type_code = Column(String(10))
    text = Column(Text)
    type_code_prefix = Column(String(10))
    serial_number = Column(Integer,
                    ForeignKey('case_file.serial_number'))


class CaseFileEventStatement(Base):
    __tablename__ = "case_file_event_statement"
    pk = Column(Integer, primary_key=True)
    code = Column(String(10))
    type = Column(String(5))
    description_text = Column(Text)
    date = Column(Date)
    number = Column(Integer)
    serial_number = Column(Integer,
                    ForeignKey('case_file.serial_number'))

###Class for Case File Event Statement Code, Type and Description
class CaseFileEventStatementCode(Base):
    __tablename__ = "case_file_event_statement_code"
    pk = Column(Integer, primary_key = True)
    code = Column(String(10))
    type = Column(String(5))
    description_text = Column(Text)
###

class Classification(Base):
    __tablename__ = "classification"
    pk = Column(Integer, primary_key=True)
    international_code = Column(String(3), ForeignKey('ref_nice_code.code'))
    classification_entry_number = Column(Integer)
    status_code = Column(String(10))
    status_date = Column(Date)
    first_use_anywhere_date = Column(String(10))
    first_use_in_commerce_date = Column(String(10))
    primary_code = Column(String(5))
    serial_number = Column(Integer,
                    ForeignKey('case_file.serial_number'))

###Class for mapping USPTO Codes to Classification International Codes
class USPTO_Classification(Base):
    __tablename__ = "map_uspto_classification"
    pk = Column(Integer, primary_key = True)
    classification_entry_number = Column(Integer)
    us_code = Column(String(10), ForeignKey('ref_uspto_code.code'))
    serial_number = Column(Integer,
                    ForeignKey('case_file.serial_number'))

###Class of Owners Change in schema accordingly
class Owner(Base):
    __tablename__ = "owner"
    pk = Column(Integer, primary_key=True)
    legal_entity_type_code = Column(String(10))
    entity_statement = Column(Text)
    party_name = Column(Text)
    address_1 = Column(String(100))
    nationality_state = Column(String(10))
    nationality_country = Column(String(10))
    nationality_other = Column(String(10))
    city = Column(String(100))
    state = Column(String(10))
    postcode = Column(String(10))
    dba_aka_text = Column(Text)
    composed_of_statement = Column(Text)
    name_change_explaination = Column(Text)
    country = Column(String(10))

'''
class MadridInternationalFilingRequests(Base):
    __tablename__ = "madrid_international_filing_requests"
    pk = Column(Integer, primary_key = True)
    entry_number =
    reference_number = Column(Integer)
    original_filing_date_uspto = Column(Date)
    international_status_code = Column(String(3))
    international_status_date = Column(Date)
    irregularity_reply_by_date = Column(Date)
    international_renewal_date = Column(Date)
    madrid_history_event = Column(Text)
    code = Column(String(10))
    description_text = Column(Text)
    date = Column(Date)
    entry_number = Column(Integer)
    serial_number = Column(Integer, ForeignKey('case_file.serial_number'))
'''

###Class to map owners to case files
class CaseFileOwner(Base):
    __tablename__ = "map_case_file_owner"
    pk = Column(Integer, primary_key = True)
    case_file = Column(Integer, ForeignKey('case_file.serial_number'))
    owner = Column(Integer, ForeignKey('owner.pk'))
    entry_no = Column(Integer)
    party_type = Column(Integer)

class Correspondent(Base):
    __tablename__ = "correspondent"
    pk = Column(Integer, primary_key=True)
    address_1 = Column(String(50))
    address_2 = Column(String(50))
    address_3 = Column(String(50))
    address_4 = Column(String(50))
    address_5 = Column(String(50))
    email_1 = Column(String(100))
    email_2 = Column(String(100))


#Class to map design searches
class DesignSearches(Base):
    __tablename__ = "design_searches"
    pk = Column(Integer, primary_key = True)
    code = Column(Integer)
    serial_number = Column(Integer, ForeignKey('case_file.serial_number'))

#Class to map international registration
class InternationalRegistration(Base):
    __tablename__ = "international_registration"
    pk = Column(Integer, primary_key = True)
    international_registration_number = Column(Integer)
    international_registration_date = Column(Date)
    international_publication_date = Column(Date)
    international_renewal_date = Column(Date)
    auto_protection_date = Column(Date)
    international_death_date = Column(Date)
    international_status_code = Column(String(3))
    international_status_date = Column(Date)
    priority_claimed_in = Column(Boolean)
    priority_claimed_date = Column(Date)
    first_refusal_in = Column(Boolean)
    serial_number = Column(Integer, ForeignKey('case_file.serial_number'))

##Class to map foreign applications
class ForeignApplications(Base):
    __tablename__ = "foreign_applications"
    pk = Column(Integer, primary_key = True)
    filing_date = Column(Date)
    registration_date = Column(Date)
    registration_expiration_date = Column(Date)
    registration_renewal_date = Column(Date)
    registration_renewal_expiration_date = Column(Date)
    entry_number = Column(Integer)
    application_number = Column(String(12))
    country = Column(String(3))
    other = Column(String(3))
    registration_number = Column(String(12))
    renewal_number = Column(String(12))
    foreign_priority_claim_in = Column(Boolean)
    serial_number = Column(Integer, ForeignKey('case_file.serial_number'))

class Processed(Base):
    __tablename__ = "processed"
    pk = Column(Integer, primary_key=True)
    processed = Column(Boolean)
    kind = Column(String(10))
    filename = Column(String(100))
    last_index = Column(Integer)
    upload_date = Column(Date)
    processed_date = Column(Date)

    def __init__(self, filename, kind, upload_date):
        self.filename = filename
        self.kind = kind
        self.upload_date = upload_date
        self.processed = False
        self.last_index = 0


def load_status_code():
    status_code_list = []
    with open("reference_tables/header_status_code.csv", encoding = "UTF-8", errors = "ignore") as csvfile:
        csvreader = csv.reader(csvfile)
        next(csvreader, None)  # skip the headers
        for row in csvreader:
            code_instance = StatusCode()
            code_instance.code = row[0]
            code_instance.status = row[1]
            code_instance.definition = row[2]
            status_code_list.append(code_instance)
    session.bulk_save_objects(status_code_list)
    session.commit()


class CountryCode(Base):
    __tablename__ = "ref_country_code"
    code = Column(String(10), primary_key=True)
    description = Column(String(50))


def load_country_code():
    country_code_list = []
    with open("reference_tables/country-code.csv", encoding = "UTF-8") as csvfile:
        csvreader = csv.reader(csvfile)
        next(csvreader, None)
        for row in csvreader:
            c = CountryCode()
            c.code = row[0][0:2]
            c.description = row[1]
            country_code_list.append(c)
    session.bulk_save_objects(country_code_list)
    session.commit()


t_party_type = Table("ref_party_type", metadata,
                     Column("code", Integer),
                     Column("definition", String(100)))


def load_party_type():
    party_type_list = []
    with open("reference_tables/party-type.csv", encoding = "ISO-8859-1") as csvfile:
        csvreader = csv.reader(csvfile)
        next(csvreader, None)
        for row in csvreader:
            party_type_list.append({
                "code": row[0],
                "definition": row[1]})
    ins = t_party_type.insert()
    conn.execute(ins, party_type_list)

class DesignSearchesCode(Base):
    __tablename__ = "ref_design_searches_code"
    pk = Column(Integer, primary_key = True)
    definition = Column(Text)
    code = Column(String(10))

class LegalEntityTypeCode(Base):
    __tablename__ = "ref_legal_entity_type_code"
    code = Column(Integer, primary_key=True)
    definition = Column(String(100))

def load_design_searches_code():
    entity_type_list = []
    with open("reference_tables/design-search-codes.csv", encoding = "utf-8", errors = "ignore") as csvfile:
        csvreader = csv.reader(csvfile)
        next(csvreader, None)
        next(csvreader, None)
        for row in csvreader:
            if(row[0] == ""):##Skip some empty rows formed during scrapping
                continue
            desg_ins = DesignSearchesCode()
            desg_ins.code = row[1]
            desg_ins.definition = row[0]
            entity_type_list.append(desg_ins)
    session.bulk_save_objects(entity_type_list)
    session.commit()

def load_legal_entity_type_code():
    entity_type_list = []
    with open("reference_tables/legal-entity-type-code.csv", encoding = "ISO-8859-1") as csvfile:
        csvreader = csv.reader(csvfile)
        next(csvreader, None)
        for row in csvreader:
            legal_ins = LegalEntityTypeCode()
            legal_ins.code = row[0]
            legal_ins.definition = row[1]
            entity_type_list.append(legal_ins)
    session.bulk_save_objects(entity_type_list)
    session.commit()

def load_uspto_code():
    code_list = []
    with open("reference_tables/uspto-codes.csv", errors = "ignore") as csvfile:
        csvreader = csv.reader(csvfile)
        next(csvreader, None)
        for row in csvreader:
            uspto_code_ins = USPTOCode()
            length = len(row[0]) #Used to add 0 padding
            tmpStr = ''
            if(row[0] != "A" and row[0] != "B"):
                for i in range(0, 3 - length):
                    tmpStr = tmpStr + '0'
            uspto_code_ins.code = tmpStr + row[0]
            uspto_code_ins.definition = row[1]
            code_list.append(uspto_code_ins)
    session.bulk_save_objects(code_list)
    session.commit()

def load_nice_code():
    code_list = []
    with open("reference_tables/nice-codes.csv", errors = "ignore") as csvfile:
        csvreader = csv.reader(csvfile)
        next(csvreader, None)
        for row in csvreader:
            nice_code_ins = NiceCode()
            length = len(row[0]) #Used to add 0 padding
            tmpStr = ''
            if(row[0] != "A" and row[0] != "B"):
                for i in range(0, 3 - length):
                    tmpStr = tmpStr + '0'
            nice_code_ins.code = tmpStr + row[0]
            nice_code_ins.definition = row[1]
            code_list.append(nice_code_ins)
    session.bulk_save_objects(code_list)
    session.commit()

def create_tables():
    if not engine.dialect.has_table(engine, "ref_status_code"):
        print("Creating tables")
        Base.metadata.create_all(engine)
        print("Created")
        load_status_code()
        print("Loaded StatusCodes")
        load_country_code()
        print("Loaded CountryCode")
        load_party_type()
        print("Loaded PartyType")
        load_legal_entity_type_code()
        print("Loaded LegalEntityTypeCode")
        load_uspto_code()
        print("Loaded USPTOCode")
        load_nice_code()
        print("Loaded NiceCode")
        load_design_searches_code()
        print("Loaded DesignSearchesCode")


def drop_tables():
    print("Drop table")
    Base.metadata.drop_all(engine)
    print("Dropped")


def restart_tables():
    drop_tables()
    create_tables()

if __name__ == "__main__":
    restart_tables()
    #create_tables()
